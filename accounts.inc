<?php

    function finance_add_account_form() {
        
        $form['account_name'] = array(
            '#type' => 'textfield',
            '#title' => t('Account Name'),
            '#default_value' => '',
            '#description' => t('The name of the account'),
            '#required' => TRUE,
        );
        
        $form['account_type'] = array(
            '#type' => 'select',
            '#title' => t('Account Type'),
            '#default_value' => 'Account Type',
            '#description' => t('The type of the account.'),
            //grab these from database
            '#options' => finance_get_account_types(),
            '#required' => TRUE,
        );
        
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Save'),
            '#submit' => array('finance_add_account_submit'),
        );

        return $form;
    }
    
    function finance_add_account_submit($form, &$form_state){
        db_insert('finance_accounts')
            ->fields(array(
            'id' => 0, 
            'description' => $form_state['values']['account_name'],
            'account_type' => $form_state['values']['account_type'], 
        ))->execute();
        
        drupal_set_message('Account added successfully!');        
    }
    
    function finance_view_account_form(){
        
        $result = finance_get_accounts();
        
        $vars['header'] = array('Account Name','Account Type');
        $vars['empty'] = 'Empty List - No Entries';
        $vars['rows'] = array();
        
        foreach($result as $record){
            array_push($vars['rows'], array(l($record->description, 'finance/transaction/'.$record->id), $record->fat_description));
        }
        
        $form['accounts'] = array(
            '#type' => 'markup',
            '#markup' => theme('table', $vars),
            '#weight' => 10,
        );
        
        return $form;
    }
    
    function finance_edit_account_form(){
        $result = finance_get_accounts(arg(3));
        
        foreach($result as $record){
            $form['account_name'] = array(
                '#type' => 'textfield',
                '#title' => t('Account Name'),
                '#default_value' => $record->description,
                '#description' => t('The name of the account'),
                '#required' => TRUE,
            );

            $form['account_type'] = array(
                '#type' => 'select',
                '#title' => t('Account Type'),
                '#default_value' => $record->fat_id,
                '#description' => t('The type of the account.'),
                '#options' => finance_get_account_types($record->fat_description),
                '#required' => TRUE,
            );
        }
        
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Save'),
            '#submit' => array('finance_edit_account_submit'),
        );

        return $form;
    }
    
    function finance_edit_account_submit($form, &$form_state){
        db_update('finance_accounts')
            ->fields(array(
            'description' => $form_state['values']['account_name'],
            'account_type' => $form_state['values']['account_type'], 
        ))
                ->condition('id', arg(3), '=')
                ->execute();
        
        drupal_set_message('Account edited successfully!');        
    }
    
    function finance_get_account_types($id = 0){
        $descriptions = array();
        $ids = array();
        
        $query = db_select('finance_account_types', 'fat');
        $query->fields('fat', array('id', 'description'))
                ->orderBy('id', 'ASC');
        
        $result = $query->execute();
        
        foreach($result as $record){
            array_push($descriptions, $record->description);
            array_push($ids, $record->id);
        }
        
        return array_combine($ids, $descriptions);
    }
    
    function finance_get_accounts($id = 0){
        $query = db_select('finance_accounts', 'fa');
        $query->fields('fa', array('id', 'description'));
        $query->join('finance_account_types', 'fat', 'fa.account_type = fat.id');
        $query->addField('fat', 'description');
        $query->addField('fat', 'id');
        $query->orderBy('fa.id', 'ASC');
        
        if($id){
            $query->condition('fa.id', $id, '=');
        }
        
        $result = $query->execute();
        
        return $result;
    }
?>