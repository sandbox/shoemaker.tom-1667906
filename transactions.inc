<?php

    function finance_add_transaction_form() {
        $form['transaction_date'] = array(
            '#type' => 'textfield',
            '#title' => t('Date'),
            '#default_value' => '',
            '#description' => t('The date of the transaction'),
            '#required' => TRUE,
        );
        
        $form['transaction_description'] = array(
            '#type' => 'textfield',
            '#title' => t('Transaction Description'),
            '#default_value' => '',
            '#description' => t('The name of the transaction'),
            '#required' => TRUE,
        );
        
        $form['transaction_category'] = array(
            '#type' => 'textfield',
            '#title' => t('Transaction Category'),
            '#default_value' => '',
            '#description' => t('The category of the transaction'),
            '#required' => TRUE,
        );
        
        $form['transaction_amount'] = array(
            '#type' => 'textfield',
            '#title' => t('Amount'),
            '#default_value' => '',
            '#description' => t('The amount of the transaction'),
            '#required' => TRUE,
        );
        
        $form['transaction_type'] = array(
            '#type' => 'select',
            '#title' => t('Type'),
            '#description' => t('The type of the transaction.'),
            '#options' => finance_get_transaction_types(),
            '#required' => TRUE,
        );
        
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Save'),
            '#submit' => array('finance_add_transaction_submit'),
        );

        return $form;
    }
    
    function finance_add_transaction_submit($form, &$form_state){
        $query = db_select('finance_categories', 'fc')
                ->fields('fc', array('id', 'name', 'description'))
                ->orderBy('id', 'ASC')
                ->condition('name', $form_state['values']['transaction_category'], '=');
        
        $result = $query->execute();
        
        foreach($result as $record){
            $category_id = $record->id;
        }
        
        if($category_id > 0){
            db_insert('finance_transactions')
                ->fields(array(
                'id' => 0, 
                'account_id' => arg(3),
                'category_id' => $category_id,
                'description' => $form_state['values']['transaction_description'],
                'type' => $form_state['values']['transaction_type'],
                'date' => strtotime($form_state['values']['transaction_date']),
                'amount' => $form_state['values']['transaction_amount'],
            ))->execute();

            drupal_set_message('Transaction added successfully!');        
        }
        else{
            drupal_set_message('Invalid Category');
        }
    }
    
    function finance_view_transaction_form(){
        
        $result = finance_get_transactions(arg(2));
        
        $vars['header'] = array('Date','Type','Description','Category','Amount');
        $vars['empty'] = 'Empty List - No Entries';
        $vars['rows'] = array();
        
        foreach($result as $record){
            array_push($vars['rows'], array(date('m/d/Y',$record->date), $record->type, $record->description, $record->fc_description, $record->amount));
        }
        
        $form['accounts'] = array(
            '#type' => 'markup',
            '#markup' => theme('table', $vars),
            '#weight' => 10,
        );
        
        return $form;
    }
    
    function finance_edit_transaction_form(){
        $result = finance_get_transactions(arg(4));
        
        foreach($result as $record){
            $form['transaction_date'] = array(
                '#type' => 'textfield',
                '#title' => t('Date'),
                '#default_value' => $record->date,
                '#description' => t('The date the transaciton occured.'),
                '#required' => TRUE,
            );

            $form['transaction_type'] = array(
                '#type' => 'select',
                '#title' => t('Type'),
                '#default_value' => $record->type,
                '#description' => t('The type of the transaction.'),
                '#options' => finance_get_transaction_types($record->type),
                '#required' => TRUE,
            );
            
            $form['transaction_description'] = array(
                '#type' => 'textfield',
                '#title' => t('Description'),
                '#default_value' => $record->description,
                '#description' => t('The description of the transaction.'),
                '#required' => TRUE,
            );
            
            $form['transaction_category'] = array(
                '#type' => 'textfield',
                '#title' => t('Category'),
                '#default_value' => $record->category_id,
                '#description' => t('The category of the transaction.'),
                '#required' => TRUE,
            );
            
            $form['transaction_amount'] = array(
                '#type' => 'textfield',
                '#title' => t('Amount'),
                '#default_value' => $record->amount,
                '#description' => t('The amount of the transaction.'),
                '#required' => TRUE,
            );
        }
        
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Save'),
            '#submit' => array('finance_edit_account_submit'),
        );

        return $form;
    }
    
    function finance_edit_transaction_submit($form, &$form_state){
        db_update('finance_accounts')
            ->fields(array(
            'date' => $form_state['values']['transaction_date'],
            'type' => $form_state['values']['transaction_type'],
            'category_id' => $form_state['values']['transaction_category'],
            'amount' => $form_state['values']['transaction_amount'],
            'description' => $form_state['values']['transaction_description'],
        ))
                ->condition('id', arg(3), '=')
                ->execute();
        
        drupal_set_message('Transaction edited successfully!');        
    }
    
    function finance_get_transaction_types($id = 0){
        $descriptions = array(1,2);
        $ids = array(1,2);
        
        /*$query = db_select('finance_account_types', 'fat');
        $query->fields('fat', array('id', 'description'))
                ->orderBy('id', 'ASC');
        
        $result = $query->execute();
        
        foreach($result as $record){
            array_push($descriptions, $record->description);
            array_push($ids, $record->id);
        }*/
        
        return array_combine($ids, $descriptions);
    }
    
    function finance_get_transactions($acct_id, $id = 0){
        $query = db_select('finance_transactions', 'ft');
        $query->fields('ft', array('id', 'account_id', 'description', 'date', 'type', 'amount'));
        $query->join('finance_categories', 'fc', 'ft.category_id = fc.id');
        $query->addField('fc', 'description');
        $query->orderBy('ft.id', 'ASC');
        $query->condition('ft.account_id', $acct_id, '=');
        
        if($id){
            $query->condition('fa.id', $id, '=');
        }
        
        $result = $query->execute();
        
        return $result;
    }
?>