<?php

    function finance_add_category_form() {
        $form['category_name'] = array(
            '#type' => 'textfield',
            '#title' => t('Category Name'),
            '#default_value' => '',
            '#description' => t('The name of the category.'),
            '#required' => TRUE,
        );
        
        $form['category_description'] = array(
            '#type' => 'textfield',
            '#title' => t('Category Description'),
            '#description' => t('The description of the category'),
            '#required' => TRUE,
        );
        
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Save'),
            '#submit' => array('finance_add_category_submit'),
        );

        return $form;
    }
    
    function finance_add_category_submit($form, &$form_state){
        db_insert('finance_categories')
            ->fields(array(
            'id' => 0, 
            'name' => $form_state['values']['category_name'],
            'description' => $form_state['values']['category_description'], 
        ))->execute();
        
        drupal_set_message('Category added successfully!');        
    }
    
    function finance_view_category_form(){
        
        $result = finance_get_categories();
        
        $vars['header'] = array('Category Name','Category Description');
        $vars['empty'] = 'Empty List - No Entries';
        $vars['rows'] = array();
        
        foreach($result as $record){
            array_push($vars['rows'], array($record->name, $record->description));
        }
        
        $form['categories'] = array(
            '#type' => 'markup',
            '#markup' => theme('table', $vars),
            '#weight' => 10,
        );
        
        return $form;
    }
    
    function finance_edit_category_form(){
        $result = finance_get_categories(arg(3));
        
        foreach($result as $record){
            $form['category_name'] = array(
                '#type' => 'textfield',
                '#title' => t('Category Name'),
                '#default_value' => $record->name,
                '#description' => t('The name of the category'),
                '#required' => TRUE,
            );

            $form['category_description'] = array(
                '#type' => 'textfield',
                '#title' => t('Category Description'),
                '#default_value' => $record->description,
                '#description' => t('The description of the category'),
                '#required' => TRUE,
            );
        }
        
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Save'),
            '#submit' => array('finance_edit_category_submit'),
        );

        return $form;
    }
    
    function finance_edit_category_submit($form, &$form_state){
        db_update('finance_category')
            ->fields(array(
            'description' => $form_state['values']['category_name'],
            'account_type' => $form_state['values']['category_description'], 
        ))
                ->condition('id', arg(3), '=')
                ->execute();
        
        drupal_set_message('Category edited successfully!');        
    }
    
    function finance_get_categories($id = 0){
        $query = db_select('finance_categories', 'fc');
        $query->fields('fc', array('id', 'name', 'description'));
        $query->orderBy('fc.id', 'ASC');
        
        if($id){
            $query->condition('fc.id', $id, '=');
        }
        
        $result = $query->execute();
        
        return $result;
    }

?>
